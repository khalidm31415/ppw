from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'To be or not to be, that is the question'

def index(request):
    response = {'name': mhs_name, 'content': '"%s"' %landing_page_content}
    return render(request, 'index_lab2.html', response)