from django.conf.urls import url
from .views import *

urlpatterns = [
         url(r'^$', index, name='index'),
         url(r'^add-friend/$', add_friend, name='add-friend'),
         url(r'^validate-npm/$', validate_npm, name='validate-npm'),
         url(r'^delete-friend/(?P<pk>\d+)/$', delete_friend, name='delete-friend'),
         url(r'^get-friend-list/$', friend_list_json, name='get-friend-list'),
         url(r'^daftar-teman/$', friend_list, name='friend-list')
]
