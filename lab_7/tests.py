from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Friend

# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab_7_url_exists(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Friend.objects.create(friend_name='Khalid Muhammad', npm='1406600136')

        # Retrieving all available activity
        counting_all_available_todo = Friend.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)