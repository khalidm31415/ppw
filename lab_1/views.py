from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Khalid Muhammad'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1996, 11, 15)  # TODO Implement this, format (Year, Month, Date)
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    curr_year = int(datetime.now().strftime("%Y"))
    return curr_year - birth_year  # TODO implement this
